## BPCG
Ben's Platform Code Generator - A system for generating application code for first-class languages and patterns for specific platforms. Translator.

### Calculator Example
**app-structure**
```
views {
  main: "main-layout"
  view {
    id: "main-layout"
    layout-file: "main-layout"
    controller: "main-view-controller"
  }
}
```

**main-layout**
```
main_layout {
  grid_layout {
    fill_parent: true
    rows: 4
    columns: 4
    row {
      button {
        text: "1"
      }
      button {
        text: "2"
      }
      button {
        text: "3"
      }
      button {
        text: "+"
      }
    }
  }
}
```
**main-view-controller**
